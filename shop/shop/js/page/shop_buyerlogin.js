



var countdown=60;
function sendemail(){
    var obj = $('#codebtn');
    settime(obj);
    
    }
function settime(obj) { //发送验证码倒计时
    if (countdown == 0) { 
        obj.attr('disabled',false); 
        //obj.removeattr("disabled"); 
        obj.val("获取验证码");
        countdown = 60; 
        return;
    } else { 
        obj.attr('disabled',true);
        obj.val("重新发送(" + countdown + ")");
        countdown--; 
    } 
setTimeout(function() { 
    settime(obj) }
    ,1000) 
}



/**
 * @author hdx
 * @date 2018/6/8
 * @Description: 登录页面js
 */
;(function(w,u){
    "use strict"
    var _window = "";
    //参数
    var parameter = {
        prevPosition : 221
    }
    var shop_buyerlogin = {
        init :function(){
            this.bind();
        },
        defaultEvent:function(){
        },
        bind:function(){
            $(".login-btn").on("click",this.eventList.login);
        },
        eventList:{
            login:function(){
                if(!shop_buyerlogin.eventList.checkDate()){
                    return;
                }
                layer.alert("登陆成功",{icon: 1, title:'提示'},function(){
                    window.location.href = 'index.html';
                })
            },
            checkDate:function(){
                var loginname = $(".loginname").val();
                var password = $(".password").val();
                var code = $(".code").val();
                if(!loginname){
                    layer.alert("请输入登录名",{icon: 2, title:'提示'});
                    return false;
                }
                if(!password){
                    layer.alert("请输入密码",{icon: 2, title:'提示'});
                    return false;
                }
                if(!code){
                    layer.alert("请输入验证码",{icon: 2, title:'提示'});
                    return false;
                }
                return true;

            }
        },
        interfaceList:{

        }
    }
    _window =  (function(){ return this||(0,eval)('this')}());
    !('shop_buyerlogin' in _window) &&  (_window.shop_buyerlogin = shop_buyerlogin);
})(window);
shop_buyerlogin.init();
