/**
 * @author sdp
 * @date 2018/6/20
 * @Description:登录页面js
 */
(function () {
   var data = {
       "username" : '',
       "password" : '',
       "code" : '',
       "getCodeFlag" : true
   }

   var bindtap = {

       bind: function () {
           //点击登录按钮
           $(".loginButton").on("click", this.eventList.submit)
           $(".getCode").on("click", this.eventList.getCode)
       },
       eventList: {
           //登录
           submit: function () {
               data.username = $(".username").val()
               data.password = $(".password").val()
               data.code = $(".code").val()
               if(!data.username){
                   console.log("用户名不能为空")
                   return false
               }
               if(!data.password){
                   console.log("密码不能为空")
                   return false
               }
               if(!data.code){
                   console.log("验证码不能为空")
                   return false
               }
               console.log("已发送")
           },
           // 获取验证码60s
           getCode: function () {
               var codeButton = $(".getCode");
               if(data.getCodeFlag){
                   data.getCodeFlag = false;
                   var time=60;
                   var t = setInterval(function () {
                       time--;
                       codeButton.html(time+"秒");
                       if (time==0) {
                           clearInterval(t);
                           codeButton.html("重新获取");
                           data.getCodeFlag=true;
                           codeButton.removeClass("msgs1");
                       }
                   },1000)
               }
           }
       }
   }
   var _window = (function () {
       return this||(0,eval)('this')
   })(); ! ('bindtap' in window) && (_window.bindtap = bindtap)
})()

bindtap.bind();