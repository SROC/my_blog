/**
 * @author zsy
 * @date 2018/6/10
 * @Description:
 * index  首页js
 */
;(function (w,u) {
    var _window = "";
    var parameter = {

    };
    var indexJs = {
        // 初始化
        init:function () {
            this.defaultEvent();
            this.bind();
            this.evevts.createSwiper();
         },
        defaultEvent:function () {
           _html.include.header();
           _html.include.footer();
           _html.include.nav();
           _html.init();
         },
        bind:function () {
            $('.tabs span').on('click',this.evevts.Tabs)
            $('.hot_lists ul li').on('click',this.evevts.addColor)
        },
        evevts:{
            // 通能切换
            Tabs:function () {
                var $index = $(this).index();
                if($index == 0){
                    $('.applicationes').removeClass('show');
                    $('.trades').removeClass('hide');
                    $('.trades').addClass('show');
                    $('.applicationes').addClass('hide');
                }else {
                    $('.applicationes').removeClass('hide');
                    $('.trades').removeClass('show');
                    $('.applicationes').addClass('show');
                    $('.trades').addClass('hide');
                }
            },
            // 添加颜色
            addColor:function () {
                $('.hot_lists ul li').removeClass('orange')
                $(this).addClass('orange')
            },
            createSwiper:function(){
                var swiper = new Swiper('.swiper-container', {
                    spaceBetween: 50,
                    slidesPerView: 1,
                    loop : true,
                    pagination : '.pagination',
                    grabCursor: true,
                    paginationClickable: true,
                    paginationAsRange : true,
                    autoplay : 3500,
                });
                parameter.swiper = swiper;
            }
        }
    };
    _window =  (function(){ return this||(0,eval)('this')}());
    !('indexJs' in _window) &&  (_window.indexJs = indexJs);
})(window);
indexJs.init();