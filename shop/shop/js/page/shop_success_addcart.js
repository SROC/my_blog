/**
 * @author hdx
 * @date 2018/6/8
 * @Description: 购物车页面js
*/
;(function(w,u){
    "use strict"
    var _window = "";
    //参数
    var parameter = {
        prevPosition : 221
    }

    var data = {
        mayNeedList :[
            {
                "imgUrl" : 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
                "title" : '青岛海尔有限公司2',
                "price" : '3000'
            },
            {
                "imgUrl" : 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
                "title" : '青岛海尔有限公司2',
                "price" : '3000'
            },
            {
                "imgUrl" : 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
                "title" : '青岛海尔有限公司2',
                "price" : '3000'
            },
            {
                "imgUrl" : 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
                "title" : '青岛海尔有限公司2',
                "price" : '3000'
            }
        ]
    }
    var shop_success_addcart = {
        init :function(){
            this.defaultEvent();
            this.bind();
        },
        defaultEvent:function(){
            //共同页面加载
            _html.include.header();
            _html.include.nav();
            _html.include.footer();

            if(data.mayNeedList.length != 0){
                _html.include.mayNeed({list:data.mayNeedList});
            }else{
                $(".mayNeed").hide()
            }
            //共同初始化事件加载
            _html.init();
        },
        bind:function(){

        },
        eventList:{

        },
        interfaceList:{

        }
    }
    _window =  (function(){ return this||(0,eval)('this')}());
    !('shop_success_addcart' in _window) &&  (_window.shop_success_addcart = shop_success_addcart);
})(window);
shop_success_addcart.init();
