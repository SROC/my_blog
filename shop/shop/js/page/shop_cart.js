/**
 * @author hdx
 * @date 2018/6/8
 * @Description: 购物车页面js
*/
;(function(w,u){
    "use strict"
    var _window = "";
    //参数
    var parameter = {
        prevPosition : 221
    }

    var data = {
        productList : [
            {
                "title" : '青岛海尔有限公司',
                "productTitle" : '海尔变频空调',
                "piece" : '1.5匹',
                "imgUrl" : 'https://ss2.bdstatic.com/8_V1bjqh_Q23odCf/pacific/1599047659.jpg',
                "price" : '3000'
            },
            {
                "title" : '青岛海尔有限公司2',
                "productTitle" : '海尔变频空调2',
                "piece" : '1.5匹',
                "imgUrl" : 'https://ss2.bdstatic.com/8_V1bjqh_Q23odCf/pacific/1599047659.jpg',
                "price" : '3000'
            },
            {
                "title" : '青岛海尔有限公司3',
                "productTitle" : '海尔变频空调3',
                "piece" : '1.5匹',
                "imgUrl" : 'https://ss2.bdstatic.com/8_V1bjqh_Q23odCf/pacific/1599047659.jpg',
                "price" : '3000'
            }
        ],
        mayNeedList :[
            {
                "imgUrl" : 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
                "title" : '青岛海尔有限公司2',
                "price" : '3000'
            },
            {
                "imgUrl" : 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
                "title" : '青岛海尔有限公司2',
                "price" : '3000'
            },
            {
                "imgUrl" : 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
                "title" : '青岛海尔有限公司2',
                "price" : '3000'
            },
            {
                "imgUrl" : 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
                "title" : '青岛海尔有限公司2',
                "price" : '3000'
            }
        ]
    }


    var shop_cart = {
        init :function(){
            this.defaultEvent();
            this.bind();
        },
        defaultEvent:function(){

            //共同页面加载
            _html.include.header();
            _html.include.nav();
            _html.include.footer();
            if(data.mayNeedList.length != 0){
                _html.include.mayNeed({list:data.mayNeedList});
            }else{
                $(".mayNeed").hide()
            }

            //自页面加载
            this.eventList.loading();
            _html.init();

            //初始化价格
            this.eventList.priceAndNum();

        },
        bind:function(){
            //加减按钮
            $(".btn-opera").on("click",this.eventList.opera);
            //全选勾选事件
            $("input.allcheck").on("change",this.eventList.allcheck);
            //单机勾选事件
            $("input.check").on("change",this.eventList.check);
            //数量变化函数
            $(".shownum input").on("change",this.eventList.changenum);
            //删除事件
            $(".opera .del").on("click",this.eventList.del);
            //删除全部事件
            $(".delAll").on("click",this.eventList.delAll);
            //结算
            $(".account").on("click",this.eventList.account);
            //加入购物车
            $(".addCart").on("click",this.eventList.addCart);

        },
        eventList:{
            opera:function(){
                var _this = $(this);
                var num = +_this.siblings(".shownum").find("input").val();
                var temp = "";
                if(_this.hasClass("add")){
                    temp= ++num;
                    //如果添加
                    _this.prev().find("input").val(temp);
                    shop_cart.eventList.subtotal(_this,temp);
                }else{
                    if(num==1){
                        return;
                    }
                    temp= --num;
                    //如果添加
                    _this.next().find("input").val(temp);
                    shop_cart.eventList.subtotal(_this,temp);
                }
                shop_cart.eventList.priceAndNum();
            },
            subtotal:function(aim,num){
                var _num = $(aim).closest(".num");
                var singleprice =+_num.prev().text();
                _num.next().text(singleprice * num);

            },
            allcheck:function(){
                var flag = $(this).is(":checked");
                var list = $("input.check");
                var listall = $("input.allcheck");
                if(flag){
                    for(var i=0;i<list.length; i++){
                            list[i].checked = true;
                    }
                    for(var j=0;j< listall.length ;j++){
                        listall[j].checked = true;
                    }
                }else{
                    for(var i=0;i<list.length; i++){
                        list[i].checked = false;
                    }
                    for(var j=0;j< listall.length ;j++){
                        listall[j].checked = false;
                    }
                }
                shop_cart.eventList.priceAndNum();
            },
            check:function(){
                var _this = $(this);
                var flag = _this.is(":checked");
                var checkedSize = $(".check:checked").length;
                var listall = $("input.allcheck");
                if(flag){
                    if(checkedSize==$("input.check").length){
                        for(var j=0;j< listall.length ;j++){
                            listall[j].checked = true;
                        }
                    }
                }else{
                    for(var j=0;j< listall.length ;j++){
                        listall[j].checked = false;
                    }
                }
                shop_cart.eventList.priceAndNum();
            },
            //全部价格展示
            priceAndNum:function(){
                var checkedList = $("input.check:checked");
                var num = checkedList.length;
                var allprice = 0 ;
                for(var i=0; i<num;i++){
                    var _aim = $(checkedList[i]).parents(".list-li");
                    var price = +_aim.find(".price").text() *  (+_aim.find(".num input").val());
                    allprice =  allprice + price;
                }
                $(".allprice").find("i").text(allprice);
                $(".choose").find("i").text(num);
            },
            changenum:function(){
                var num = +$(this).val();
                if(!num){
                    layer.alert("商品数量必须大于0", {icon: 2});
                    $(this).val(1);
                    num = 1;
                }
                var _parents = $(this).parents(".num");
                var single = +_parents.prev().text();
                _parents.next().text(single*num);
                shop_cart.eventList.priceAndNum();
            },
            del:function(){
                var _this = $(this);
                layer.confirm('您确定要删除此商品', {icon: 3, title:'提示'}, function(index){
                    _this.parents(".list-li").remove();
                    //调用ajax

                    layer.close(index);
                });
            },
            delAll:function(){
                var checkedList = $("input.check:checked");
                var num = checkedList.length;
                if(!num){
                    layer.alert("请至少选中一件商品！", {icon: 2});
                    return;
                }

                layer.confirm('您确定要删除选择的商品', {icon: 3, title:'提示'}, function(index){
                    for(var i=0; i<num;i++){
                        $(checkedList[i]).parents(".list-li").remove();
                    }
                    //调用ajax
                    layer.close(index);
                });
            },
            account:function(){
                var checkedList = $("input.check:checked");
                var num = checkedList.length;
                if(!num){
                    layer.alert("请至少选中一件商品！", {icon: 2});
                    return;
                }
                window.location.href = "shop_firmorder.html";
            },
            addCart:function(){
                //此处调接口

                layer.alert("已成功加入购物车！", {icon: 1});
            },
            loading:function () {
                if(data.productList.length !=0 ){
                    var productList = template('common/page/productList', {list:data.productList});
                    $(".productList").html(productList);
                    _html.eventList.lazyload();
                }
            }

        },
        interfaceList:{

        }
    }
    _window =  (function(){ return this||(0,eval)('this')}());
    !('shop_cart' in _window) &&  (_window.shop_cart = shop_cart);
})(window);

shop_cart.init();
