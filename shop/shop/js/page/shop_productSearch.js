(function(w,u) {
    var data = {
        productList : [
            {
                "title" : '青岛海尔有限公司',
                "productTitle" : '海尔变频空调',
                "piece" : '1.5匹',
                "imgUrl" : 'https://ss2.bdstatic.com/8_V1bjqh_Q23odCf/pacific/1599047659.jpg',
                "price" : '3000'
            },
            {
                "title" : '青岛海尔有限公司2',
                "productTitle" : '海尔变频空调2',
                "piece" : '1.5匹',
                "imgUrl" : 'https://ss2.bdstatic.com/8_V1bjqh_Q23odCf/pacific/1599047659.jpg',
                "price" : '3000'
            },
            {
                "title" : '青岛海尔有限公司3',
                "productTitle" : '海尔变频空调3',
                "piece" : '1.5匹',
                "imgUrl" : 'https://ss2.bdstatic.com/8_V1bjqh_Q23odCf/pacific/1599047659.jpg',
                "price" : '3000'
            }
        ],
        mayNeedList :[
            {
                "imgUrl" : 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
                "title" : '青岛海尔有限公司2',
                "price" : '3000'
            },
            {
                "imgUrl" : 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
                "title" : '青岛海尔有限公司2',
                "price" : '3000'
            },
            {
                "imgUrl" : 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
                "title" : '青岛海尔有限公司2',
                "price" : '3000'
            },
            {
                "imgUrl" : 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
                "title" : '青岛海尔有限公司2',
                "price" : '3000'
            }
        ]
    }
     var shop_productSearch = {
         init:function(){
             this.defaultEvent();
             this.bind()
         },
         defaultEvent:function(){
             //共同页面加载
             _html.include.header();
             _html.include.footer();
             _html.init();
            //加载页面模板
             this.eventList.loading($("#_sidebar"),"common/page/shop_productSearch/productSideBar",data.productList)
             this.eventList.loading($("._contentProduct_list"),"common/page/shop_productSearch/productListContent",data.productList)
             this.eventList.loading($("#topbar"),"common/page/shop_productSearch/topBar",data.productList)

         },
         bind:function () {
             $("._list_box ._content_item_bottom a").click(this.eventList.bind_gouwuche)
         },
         eventList:{
             loading:function (em,Tur,data) {
                 if(data.length !=0 ){
                     var itemlist = template(Tur, {list:data});
                     $(em).html(itemlist);
                 }else {
                     $(em).hide();
                 }
             },
             bind_gouwuche:function () {
                 layer.alert("已成功加入购物车！", {
                     icon: 1,
                     btn: "去购物车结算"
                 }, function () {
                     window.location.href = "shop_success_addcart.html"
                 })}
            },
         }
    _window =  (function(){ return this||(0,eval)('this')}());
 !('shop_productSearch' in _window) &&  (_window.shop_productSearch = shop_productSearch);
 })(window)
shop_productSearch.init();
