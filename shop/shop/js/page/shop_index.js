/**
 * @author hdx
 * @date 2018/6/8
 * @Description:
 * shop_index  商城首页js
*/
;(function(w,u){
    "use strict"
    var _window = "";
    //参数
    var parameter = {

    }
    var shop_index = {
        //初始化
        init:function(){
            this.defaultEvent();
            this.bind();
            this.eventList.createSwiper();
        },
        defaultEvent:function(){
            //共同页面加载
            _html.include.header();
            _html.include.footer();
            _html.init();
        },
        //绑定事件
        bind:function(){
            //绑定swiper事件
            $('.arrow-left').on("click",this.eventList.arrowLeft);
            $('.arrow-right').on("click",this.eventList.arrowRight);
        },
        //事件列表
        eventList:{
            //箭头函数点击事件
            arrowLeft:function(e){
                e.preventDefault()
                parameter.swiper.swipePrev()
            },
            //箭头函数点击事件
            arrowRight:function(e){
                e.preventDefault()
                parameter.swiper.swipeNext()
            },
            //swiper初始化事件
            createSwiper:function(){
                var swiper = new Swiper('.swiper-container', {
                    spaceBetween: 50,
                    slidesPerView: 1,
                    loop : true,
                    pagination : '.pagination',
                    grabCursor: true,
                    paginationClickable: true,
                    paginationAsRange : true,
                    autoplay : 3500,
                });
                parameter.swiper = swiper;
            }
        }
    }
    _window =  (function(){ return this||(0,eval)('this')}());
    !('shop_index' in _window) &&  (_window.shop_index = shop_index);
})(window);
shop_index.init();
