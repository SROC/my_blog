(function() {

	var data = {
		mayNeedList: [{
				"imgUrl": 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
				"title": '青岛海尔有限公司2',
				"price": '3000'
			},
			{
				"imgUrl": 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
				"title": '青岛海尔有限公司2',
				"price": '3000'
			},
			{
				"imgUrl": 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
				"title": '青岛海尔有限公司2',
				"price": '3000'
			},
			{
				"imgUrl": 'http://b2b.haier.com/upload/20180306/2018030628.11688696817832.jpg',
				"title": '青岛海尔有限公司2',
				"price": '3000'
			}
		]
	}

	//共同页面加载
	_html.include.header();
	_html.include.nav();
	_html.include.footer();

	if(data.mayNeedList.length != 0) {
		_html.include.mayNeed({
			list: data.mayNeedList
		});
	} else {
		$(".mayNeed").hide()
	}

	_html.init();

})()

//地区选择
//自定义配置
var config = {
	province: '#province',
	city: '#city',
	area: '#area'
};
//初始化
$(function() {
	selector(config);
});

//		显示新增地址
$(".check_add").click(function() {
	$(".newAdress").show();
});
$(".close").click(function() {
	$(".newAdress").hide();
});
$(".add_delete").click(function() {
	$(".newAdress").hide();
});
//选择地址
$(".more_adress").click(function() {
	$(".choose").show();
});
$(".down").click(function() {
	$(".choose").hide();
});
$(".choose_delete").click(function() {
	$(".choose").hide();
});
//发票
$(".add_invoice_button").click(function() {
	$(".add_invoice_all").show();
});
$(".invoice_delete").click(function() {
	$(".add_invoice_all").hide();
});
$(".save_invoice").click(function() {
	$(".add_invoice_all").hide();
});
$("#wname").click(function(){
	if($(this).prop("checked")){
	$(".new_invoice").show();
	}
});
$(".lname").click(function(){
	if($(this).prop("checked")){
	$(".new_invoice").hide();
	}
});
$("#tname").click(function(){
	if($(this).prop("checked")){
	$(".invoice_detail_special").hide();
	$(".invoice_detail_public").show();
	}
});
$("#zname").click(function(){
	if($(this).prop("checked")){
	$(".invoice_detail_special").show();
	$(".invoice_detail_public").hide();
	}
});