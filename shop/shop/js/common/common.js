;(function (w, u) {
    "use strict";
    var _window = "";
    var $$ = {
        //ajax公共方法
        ajax:function(url, data){
                var end = $.ajax({
                    headers: {
                        Accept: "application/*; charset=utf-8"
                    },
                    url: url,
                    type: data == null ? 'GET' : 'POST',
                    dataType: "json",
                    data: data == null ? '' : data,
                    async: true,
                    xhrFields: {
                        withCredentials: true
                    },
                    beforeSend: function () {
                        var index = layer.load(1);
                    },
                    success: function (resp) {
                        layer.close(index);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        layer.close(index);
                    }
                });
            return end;
        },
        //获取地址后带有的参数
        getUrlParam:function() {
            var url = location.search;
            var theRequest = new Object();
            if (url.indexOf("?") != -1) {
                var str = url.substr(1);
                var strs = str.split("&");
                for (var i = 0; i < strs.length; i++) {
                    /*theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);*/
                    theRequest[strs[i].split("=")[0]] = decodeURI(strs[i].split("=")[1]);
                }
            }
            return theRequest;
        },
        //根据id得到对象
        obj$:function() {
            return document.getElementByIdx(id);
        },
        //根据id得到对象的值
        val$:function(id) {
            var obj = document.getElementByIdx(id);
            if (obj !== null) {
                return obj.value;
            }
            return null;
        },
        //删除左边和右边空格
        trim:function(str) {
            return str.replace(/(^\s*)|(\s*$)/g, '');
        },
        //判断是否电子邮件
        isEmail:function(str) {
            if (/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/.test(str)) {
                return true
            }
            return false;
        },
        //判断是否是手机号
        isMobile:function(str) {
            if (/^[1][3,4,5,7,8,9][0-9]{9}$/.test(str)) {
                return true;
            }
            return false;

        },
        //判断是否是正确ip
        isIP:function(str) {
            var reg = /^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$/;
            if (reg.test(str)) {
                return true;
            }
            return false;
        },
        //判断是否是正确身份证号
        isCardNo:function(card) {
            // 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
            var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
            if (reg.test(card)) {
                return true;
            }
            return false;
        },
        getObjectURL:function(file) {
            var url = null;
            if (window.createObjectURL != undefined) { // basic
                url = window.createObjectURL(file);
            } else if (window.URL != undefined) { // mozilla(firefox)
                url = window.URL.createObjectURL(file);
            } else if (window.webkitURL != undefined) { // webkit or chrome
                url = window.webkitURL.createObjectURL(file);
            }
            return url;
        }

    }
    _window = (function() {
        return this || (0, eval)('this');
    }());
    !('$$' in _window) && (_window.$$ = $$);
})(window);


	
