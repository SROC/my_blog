/*TMODJS:{"version":1,"md5":"1b94f3c3b8e4018e2cbf2837509a680e"}*/
template('common/page/shop_productList/productItem',function($data,$filename
/*``*/) {
'use strict';var $utils=this,$helpers=$utils.$helpers,$each=$utils.$each,list=$data.list,value=$data.value,index=$data.index,$escape=$utils.$escape,$out='';$each(list,function(value,index){
$out+=' <div class="_produt_item"> <div class="_item_img"> <img src="';
$out+=$escape(value.imgUrl);
$out+='" alt=""> </div> <div class="_item_text"> <p class="_text_price">￥';
$out+=$escape(value.price);
$out+='</p> <p>';
$out+=$escape(value.title);
$out+='</p> <p class="_text_label"> <span>';
$out+=$escape(value.piece);
$out+='</span> <span>圆柱式空调</span> </p> <p>海尔官方旗舰店 <i class="iconfont">&#xe60f;</i></p> <span class="_text_sign">自营</span> </div> </div> ';
});
return new String($out);
});