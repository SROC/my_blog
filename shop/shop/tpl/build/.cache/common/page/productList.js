/*TMODJS:{"version":1,"md5":"dfb60b18b171469dc102445905042d12"}*/
template('common/page/productList',function($data,$filename
/*``*/) {
'use strict';var $utils=this,$helpers=$utils.$helpers,$each=$utils.$each,list=$data.list,value=$data.value,index=$data.index,$escape=$utils.$escape,$out='';$out+=' ';
$each(list,function(value,index){
$out+=' <div class="list-li"> <div class="check"><input type="checkbox" class="check"></div> <div class="pro"> <p class="title">';
$out+=$escape(value.title);
$out+='</p> <div class="detail"> <div class="left"> <div class="img-content"> <a href=""> <img data-original="http://b2b.haier.com/upload/f0a872bf-b883-4a0c-a1bd-86f3c3a71615/7b9eda99-a722-4f7c-9006-f11bae53b544.jpg" src="../images/loading.gif" class="screenImg lazy" alt=""/> </a> </div> </div> <div class="right"> <p class="proname"> <a href="">';
$out+=$escape(value.productTitle);
$out+='</a></p> <p class="prodetail"> <span class="label">匹数:</span> <span class="val">';
$out+=$escape(value.piece);
$out+='</span> </p> </div> </div> </div> <div class="price">';
$out+=$escape(value.price);
$out+='</div> <div class="num"> <p class="num-p"> <span class="minus btn-opera">-</span> <span class="shownum"><input type="text" value="1" class="" name="" onkeyup="this.value=this.value.replace(/[^\\d]/g,\'\');"></span> <span class="add btn-opera">+</span> </p> </div> <div class="tip">';
$out+=$escape(value.price);
$out+='</div> <div class="opera"><a class="del">删除</a></div> </div> ';
});
$out+=' ';
return new String($out);
});