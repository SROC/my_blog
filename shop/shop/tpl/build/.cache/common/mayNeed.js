/*TMODJS:{"version":3,"md5":"7bbd20bab93bc47a922764bddd7e68fb"}*/
template('common/mayNeed',function($data,$filename
/*``*/) {
'use strict';var $utils=this,$helpers=$utils.$helpers,$each=$utils.$each,list=$data.list,value=$data.value,index=$data.index,$escape=$utils.$escape,$out='';$out+=' <div class="need-content w"> <p class="title">您可能还需要</p> <ul> ';
$each(list,function(value,index){
$out+=' <li> <div class="left"> <a href=""><img src="';
$out+=$escape(value.imgUrl);
$out+='" alt=""></a> </div> <div class="right"> <div class="top"> <p class="proname"> <a href="">';
$out+=$escape(value.title);
$out+='</a></p> <p class="proprice">￥ ';
$out+=$escape(value.price);
$out+='</p> </div> <div class="bottom"> <span class="addCart"> <i class="iconfont icon-gouwuche"></i>加入购物车</span> </div> </div> </li> ';
});
$out+=' </ul> </div>';
return new String($out);
});