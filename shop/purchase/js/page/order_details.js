/**
 *@desc
 *@author hdx
 *@date 2018/07/10 15:23:05
 * 订单列表
 */

;(function(w,u){
    var _global = w;
    var order_details = {
        element:{
            orderstate:"0",
        },
        //初始化
        init:function(){
            this.bind();
            this.evnetList.pluginInit();
        },
        //绑定
        bind:function(){
            //发票相关
            $(".tax").on("change",this.evnetList.changeRadioFax);
            //订单跟踪
            $(".followorder").on("click",this.evnetList.followorder);
            //导出
            $(".expore").on("click",this.evnetList.expore)
        },
        //事件列表
        evnetList:{
            //切换tab
            changeRadioFax:function(val){
               switch (val){
                   case "0":
                        $(".d-padding").hide();
                        break;
                   case "1":
                       $(".d-padding").show();
                       $(".zzsfp").show();
                       break;
                   case "2":
                       $(".d-padding").show();
                       $(".zzsfp").hide();
                       break;

               }
            },
            followorder:function(){
//              layer.alert("我是订单跟踪按钮，我在order_details.js的45行");
            },
            expore:function(){
                layer.alert("我是导出按钮，我在order_details.js的51行");
            },
            pluginInit:function(){
                order_details.plugin.initForm();
            }
        },
        interfaceList:{

        },
        //插件管理
        plugin:{
            initForm:function(){
                layui.use('form', function(){
                    var form = layui.form;
                    form.on('radio(test)', function(data){
                        //发票radio
                        layer.alert("发票radio事件，我在order_details.js的67行");
                        order_details.evnetList.changeRadioFax(data.value);
                    });
                    //各种基于事件的操作，下面会有进一步介绍
                });
            }
        }
    }
    _global = (function(){
        return this||(0,eval)("this");
    }());
    !("order_details" in _global)&& (_global.order_details = order_details);
})(window);

order_details.init();
