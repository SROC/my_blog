(function () {

    var commons = $(".main-nav-commons");
    $.ajax({
        //请求方式
        type:"GET",
        //文件位置
        url:"./json/nav.json",
        //返回数据格式为json,也可以是其他格式如
        dataType: "json",
        //请求成功后要执行的函数，拼接html
        success: function(res){
            var ghtml = "";
            ghtml += '<div class="main-nav-common nav-show">';
            ghtml += '<div class="main-nav-common-title">'
            ghtml += '<div class="main-nav-common-title-icon"><img src="http://sroc.top/images/shouye.png"></div>'
            ghtml += '<div class="main-nav-common-title-text index-link selectTitle">首页</div>'
            ghtml += '</div></div></div>'
            $.each(res.navList.nav, function (i, item) {
                ghtml += '<div class="main-nav-common nav-show">';
                ghtml += '<div class="main-nav-common-title">'
                ghtml += '<div class="main-nav-common-title-icon"><img src="'+ item.icon +'"></div>'
                ghtml += '<div class="main-nav-common-title-text">'+ item.first + '</div> <div class="main-nav-common-title-showOrHide"><i class="icon iconfont">&#xe636;</i></div></div> <div class="main-nav-common-list hide">'
                $.each(item.navList, function (j, item2) {
                    ghtml += '<div class="main-nav-common-lister"><a href="'+ item2.href +'" target="main">'+ item2.title + '</a></div>'
                })
                ghtml += '</div></div></div>'
            })
            commons.html(ghtml)
            //首页的跳转
            $(".index-link").on('click',function () {
                window.location.href = './main.html'
            })
            // $(".main-nav-common-lister").eq(0).addClass("list-show")
            // 点击切换手风琴
            $(".main-nav-common").on('click',function () {
                // 如果当前状态为已展开
                if($(this).children(".main-nav-common-list").attr("class") == 'main-nav-common-list show'){
                    $(this).children(".main-nav-common-list").removeClass('show').addClass('hide')
                    $(this).find(".main-nav-common-title-showOrHide").html('<i class="icon iconfont">&#xe636;</i>')
                    // 将所有颜色变成默认色
                    // $(".main-nav-common-title-text").removeClass("selectTitle")
                }else if($(this).children(".main-nav-common-list").attr("class") == 'main-nav-common-list hide'){
                    // $(".main-nav-common-list").removeClass('show').addClass('hide')
                    // $(".main-nav-common-title-showOrHide").html('<i class="icon iconfont">&#xe636;</i>')
                    $(this).children(".main-nav-common-list").removeClass('hide').addClass('show')
                    $(this).find(".main-nav-common-title-showOrHide").html('<i class="icon iconfont">&#xe634;</i>')
                }

            })
            // 切换面包屑
            $(".main-nav-common-lister").on('click',function (event) {
                // 点击的时候切换
                $(".main-nav-common-lister").removeClass("list-show")
                $(this).addClass("list-show")
                // 点击的时候让其父级颜色加深
                $(".main-nav-common-title-text").removeClass("selectTitle")
                $(this).parent().parent().find(".main-nav-common-title-text").addClass("selectTitle")
                // 点击的时候读取当前html
                var sonText = $(this).children("a").text();
                var FatherText = $(this).parent().parent().find(".main-nav-common-title-text").text()
                console.log(FatherText)
                console.log(sonText)
                $(".navInfo-top-left-text").text(FatherText + ' / ' + sonText )

                // 阻止冒泡
                event.stopPropagation();
                event.cancelBubble = true;
            })
        }
    });



})();