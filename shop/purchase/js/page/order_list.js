/**
 *@desc
 *@author hdx
 *@date 2018/07/10 15:23:05
 * 订单列表
 */

;(function(w,u){
    var _global = w;
    var order_list1 = {
        element:{
            orderstate:"0",
        },
        //初始化
        init:function(){
            this.bind();
            this.evnetList.pluginInit();
        },
        //绑定
        bind:function(){
            $(".tab ul li").on("click",this.evnetList.changeTab)
        },
        //事件列表
        evnetList:{
            //切换tab
            changeTab:function(){
                var index = $(this).index();
                $(this).addClass("active").siblings().removeClass("active");
                switch (index){
                    case 0:
                }
            },
            pluginInit:function(){
                order_list1.plugin.initlayui("page");
                order_list1.plugin.initlaydate(".starttime");
                order_list1.plugin.initlaydate(".endtime");
            }
        },
        interfaceList:{

        },
        //插件管理
        plugin:{
            initlaydate:function(ele){
                laydate.render({
                    elem: ele
                    ,type: 'datetime'
                });
            },
            initlayui:function(ele){
                layui.use('laypage', function(){
                    var laypage = layui.laypage;

                    //执行一个laypage实例
                    laypage.render({
                        elem: 'page' //注意，这里的 test1 是 ID，不用加 # 号
                        ,count: 50 //数据总数，从服务端得到
                    });
                });
            }
        }
    }
    _global = (function(){
        return this||(0,eval)("this");
    }());
    !("order_list1" in _global)&& (_global.order_list1 = order_list1);
})(window);

order_list1.init();
