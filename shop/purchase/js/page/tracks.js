(function () {
    $(".content-selects-boxs").on('click','input', function () {
        if (this.checked){
            $("input[name='item']:checkbox").each(function(){
                $(this).attr("checked", true);
            });
        } else {
            $("input[name='item']:checkbox").each(function() {
                $(this).attr("checked", false);
            });
        }

    })
    
    
    $(".settlement-buttom").on('click', function () {
        var ids = [];
        $("input[name='item']:checked").each(function () {
            ids.push($(this).attr("data-id"));
        })
        if(ids == ""){
            alert("您没有选择任何商品");
        }else{
            var delIds = ids.join(",");
            alert("您选择的商品为"+ delIds);
        }

    })


    layui.use('laydate', function(){
        //日期范围
        laydate.render({
            elem: '#test6'
            ,range: true
        });
    })


    layui.use('laypage', function(){
        var laypage = layui.laypage;

        //执行一个laypage实例
        laypage.render({
            elem: 'page' //注意，这里的 test1 是 ID，不用加 # 号
            ,count: 15, //数据总数，从服务端得到
            layout:['prev', 'page', 'next'],
            jump: function(obj, first){
                //obj包含了当前分页的所有参数，比如：
                console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                console.log(obj.limit); //得到每页显示的条数
                //首次不执行
                if(!first){
                    console.log("当前页="+ obj.curr +", 每页显示条数=" + obj.limit + "</br>"+ " 我在returns.js第55行");
                }
            }
        });
    });

})()