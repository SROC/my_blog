/**
 * @author hdx
 * @date 2018/6/8
 * @Description: 购物车页面js
 */
;(function (w, u) {
    "use strict"
    var _window = "";
    //参数
    var parameter = {
        prevPosition: 221,
        lazyObj: {
            event: "sporty",
            placeholder: "images/loading.gif",
            effect: "fadeIn"
        }
    }
    var _html = {
        init: function () {
            this.defaultEvent();
            this.bind();
        },
        defaultEvent: function () {
            $(".purch").addClass("none");
            $(".purch").hide();
            $('.show_div').children('a').addClass("show_oneline");
            //地图定位事件
            if(!$.cookie('locationAddress')){
                this.eventList.getlocation();
            }else{
                $(".text").text($.cookie('locationAddress'));
            }

            //用户是否登陆
            this.eventList.isLogin();
        },
        bind: function () {
            //项目底部滑动条
            $(".leader .nav_child").hover(this.eventList.leaderStrat, this.eventList.leaderEnd);
            //产品分类
            $(".topsorts").hover(this.eventList.topsortsHover);
            //sorts_show效果
            $(".purch>li").hover(this.eventList.purchStart, this.eventList.purchEnd);
            //分类下div效果
            $(".purch>div").hover(this.eventList.purchDivStart, this.eventList.purchDivEnd);
            //首页产品菜单
            $(".purch li").hover(this.eventList.purchLiStart, this.eventList.purchLiEnd);
            //特别专题
            $('#seminars').hover(this.eventList.seminarsStart, this.eventList.seminarsEnd);
            //特别专题 ul hover事件
            $("#seminars ul").hover(this.eventList.seminarsUlEv);
            //allSorts 鼠标离开事件
            $(".allSorts").mouseleave(this.eventList.allSortsLeave);
            //show_oneline hover事件
            $('.show_oneline').hover(this.eventList.show_onelineStart, this.eventList.show_onelineEnd);
            //右侧固定栏事件
            $(".side ul li").hover(this.eventList.sidehoverStart, this.eventList.sidehoverEnd);
            //返回头部
            $(".footer .sidetop").on("click", this.eventList.sidetop);
            //客服移入
            $(".service-p").mouseenter(this.eventList.sidemouseenter);
            //客服移除
            $(".service-p").mouseleave(this.eventList.sidemouseleave);
        },
        eventList: {
            //活动下表事件开始
            leaderStrat: function () {
                $("#border_move").css("display", "block");
                var oldLeft = 245;
                var $index = $(this).index(".leader .nav_child");
                var widNow = $(this).width() + 40;
                for (var i = 0; i < $index; i++) {
                    oldLeft += $(".leader .nav_child").eq(i).width() + 40;
                }
                if (parameter.prevPosition > oldLeft) {
                    $("#border_move").stop().animate({
                        left: oldLeft - 15,
                        width: widNow
                    }, function () {
                        $("#border_move").stop().animate({
                            left: oldLeft,
                            width: widNow
                        })
                    })
                } else {
                    $("#border_move").stop().animate({
                        left: oldLeft + 15,
                        width: widNow
                    }, function () {
                        $("#border_move").stop().animate({
                            left: oldLeft,
                            width: widNow
                        })
                    });
                }
                parameter.prevPosition = oldLeft;
            },
            //活动下表事件结束
            leaderEnd: function () {
                $("#border_move").css("display", "none");
            },
            //产品分类hover事件
            topsortsHover: function () {
                $(".purch>div").hide();
                var pro_num = $('.allSorts .purch').children('li').length;
                if (pro_num >= 7) {
                    var last_padding = (320 / pro_num - 21.2) / 2;
                    $('.allSorts .purch li').css({
                        "paddingTop": "0" + last_padding + "px",
                        "paddingBottom": "0" + last_padding + "px"
                    });
                }
                $(".purch").show();
            },
            //产品分类hover事件开始
            purchStart: function () {
                var $index_par = $(this).index(".purch>li");
                for (var i = 0; i <= $index_par; i++) {
                    $(".purch>li").eq($index_par).siblings("div").eq(0).css({
                        "display": "block"
                    });
                }
            },
            //产品分类hover事件结束
            purchEnd: function () {
                $(".purch>div").css({"display": "none"});
            },
            //产品分类下div hover事件开始
            purchDivStart: function () {
                var $index = $(this).index(".purch>div");
                $(".purch>div").eq($index).css({
                    "display": "block"
                })
            },
            //产品分类下div hover事件结束
            purchDivEnd: function () {
                var third_show = document.getElementById("third_show");
                $(".purch>div").css({
                    "display": "none"
                });
                $(".purch .third_show").hide();
            },
            //首页产品菜单
            purchLiStart: function () {
                $(this).css({
                    "background": "#fff",
                    "borderLeft": "2px solid #0059a9",
                    "borderRight": "0"
                }).siblings("li").css("background", "#f7f7f7");
                var size = $(".purch li").size();
                var index = $(this).index();
                var minheight = 0;
                for (var j = index; j < size; j++) {
                    minheight = minheight + $(".show_div").eq(j).height();
                }
                $(this).find("a").css("color", "#007EC7");
                $(this).siblings("li").find("a").css("color", "#000");
                $(".show_div").eq(index).find("a").css("color", "#007EC7");
                $(".show_div").eq(index).siblings().find("a").css("color", "#000")
                if (minheight < 185) {
                    return false;
                }
                var height = 0;
                for (var i = index; i >= 1; i--) {
                    height = height + $(".show_div").eq(i - 1).height();
                }
                $(".shows_container").stop().animate({
                        top: -height
                    },
                    500);
            },
            purchLiEnd: function () {
                $(".purch li").css({
                    "background": "#f7f7f7",
                    "borderLeft": "0",
                    "borderRight": "1px solid #d6d6d6"
                });
                $(".purch li").find("a").css("color", "#000");
            },
            seminarsStart: function () {
                $("#seminars ul li").css("display", "block");
                $("#seminars ul").css("display", "block");
            },
            seminarsEnd: function () {
                $("#seminars ul li").css("display", "none");
                $("#seminars ul").css("display", "none")
            },
            seminarsUlEv: function () {
                $("#seminars ul li").css("display", "block");
                $("#seminars ul").css("display", "block")
            },
            allSortsLeave: function () {
                $(".purch").hide();
            },
            show_onelineStart: function () {
                $($(this)[0]).css("color", "#007ec7");
                $(this).siblings(".show_oneline").css("color", "#000");
            },
            show_onelineEnd: function () {
                $($(this)[0]).css("color", "#000");
            },
            sidehoverStart: function () {
                $(this).find(".sidebox").stop().animate({
                    "width": "124px"
                }, 200).css({
                    "opacity": "1",
                    "filter": "Alpha(opacity=100)",
                    "background": "#FF6A00"
                });
                $(this).find(".codeimg").stop().css({
                    "opacity": "1",
                    "filter": "Alpha(opacity=100)",
                    "background": "#FF6A00"
                });
                $(this).find(".floating_left").stop().css("display", "block");
            },
            sidehoverEnd: function () {
                $(this).find(".sidebox").stop().animate({
                    "width": "54px"
                }, 200).css({
                    "opacity": "0.8",
                    "filter": "Alpha(opacity=80)",
                    "background": "#0058A8"
                });
                $(this).find(".codeimg").stop().css({
                    "opacity": "0.8",
                    "filter": "Alpha(opacity=80)",
                    "background": "#0058A8"
                });
                $(this).find(".floating_left").stop().css("display", "none");
            },
            sidetop: function () {
                $('html,body').animate({
                    'scrollTop': 0
                }, 300);
            },
            sidemouseenter: function () {
                $(".serviceing").css("display", "block");
            },
            sidemouseleave: function () {
                $(".serviceing").css("display", "none");
            },
            //懒加载
            lazy: function () {
                $(".lazy").trigger("sporty")
            },
            //定位函数
            getlocation: function () {
           /*     _html.eventList.dynamicLoadJs('https://3gimg.qq.com/lightmap/components/geolocation/geolocation.min.js');*/
                $.getScript('https://3gimg.qq.com/lightmap/components/geolocation/geolocation.min.js');
                var options = {timeout: 8000};
                var geolocation = new qq.maps.Geolocation("OB4BZ-D4W3U-B7VVO-4PJWW-6TKDJ-WPB77", "myapp");
                var positionNum = 0;
                geolocation.getLocation(_html.eventList.showPosition, _html.eventList.showErr, options);
            },
            //定位回调函数
            showPosition: function (position) {
                var location = JSON.stringify(position, null, 4);
                $(".text").text(JSON.parse(location).city);
            },
            showErr: function () {
                $(".header .location a").text("定位失败");
            },
            //是否登陆
            isLogin:function(){
                if($.cookie('cookieName')){
                    //用户名展示
                    $(".header .userinfo-li").show().end().find(".userinfo").text($.cookie('cookieName'));
                    //请先登录隐藏
                    $(".header .login-li").hide();
                };
            },
            lazyload:function(){
                //触发懒加载
                setTimeout(_html.eventList.lazy, 1500);
                $(".lazy").lazyload(parameter.lazyObj);
            }
           /* dynamicLoadJs: function (url, callback) {
                var head = document.getElementsByTagName('head')[0];
                var script = document.createElement('script');
                script.src = url;
                if (typeof(callback) == 'function') {
                    script.onload = script.onreadystatechange = function () {
                        if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
                            callback();
                            script.onload = script.onreadystatechange = null;
                        }
                    };
                }
                head.appendChild(script);
            }*/
        },
        //共同页面
        include: {
            header: function () {
                var header = template('common/header', {name: "测试"});
                $("#header").html(header);
            },
            nav: function () {
                var nav = template('common/nav', {name: "测试"});
                $("#nav").html(nav);
            },
            footer: function () {
                var footer = template('common/footer', {name: "测试"});
                $("#footer").html(footer);
            },
            mayNeed: function (data) {
                var mayNeed = template('common/mayNeed', data);
                $(".mayNeed").html(mayNeed);
            },
            otherRecommend:function (data) {
                var otherRecommend = template('common/page/shop_producedetail/otherRecommend', data);
                $("#groom_pro").html(otherRecommend);
            }
        }
    }
    _window = (function () {
        return this || (0, eval)('this')
    }());
    !('_html' in _window) && (_window._html = _html);
})(window);


