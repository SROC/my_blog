/**
 *@desc
 *@author hdx
 *@date 2018/07/10 15:23:05
 * 订单列表
 */

;(function(w,u){
    var _global = w;
    var order_details = {
        element:{
            orderstate:"0",
        },
        //初始化
        init:function(){
            this.bind();
        },
        //绑定
        bind:function(){
        },
        //事件列表
        evnetList:{
            pluginInit:function(){
            }
        },
        interfaceList:{

        },
        //插件管理
        plugin:{
        }
    }
    _global = (function(){
        return this||(0,eval)("this");
    }());
    !("order_details" in _global)&& (_global.order_details = order_details);
})(window);

order_details.init();
