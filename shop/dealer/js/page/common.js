(function () {
    // 获取当前页面的高度
    function setParentHeight() {
        var arrheight = document.body.offsetHeight;
        $(window.parent.$("#messagediv").text(arrheight));
    }
    function setParentTitle() {
        var arrtitile = $("title").text()
        $(window.parent.$(".navInfo-top-left-text").text(arrtitile));
    }

    setParentHeight()
    setParentTitle()
})()