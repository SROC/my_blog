;(function (w, u) {
    var _global = w;
    var postTrand = {
        init: function () {
            this.bind()
            this.eventList.PluginInit()
        },
        bind: function () {
            $("._itemText").on("click", this.eventList.check)
            $(".text_show").on("click", function (event) {
                event.stopPropagation();
            })
            $(".postTrade_submit").on("click",this.eventList.postrade)
        },
        eventList: {
            // 点击选中
            check: function () {
                if ($(".text_check").hasClass("hide")) {
                    $(".text_check").removeClass("hide");
                    return false;
                } else {
                    $(".text_check").addClass("hide");
                }
            },
            PluginInit: function () {
                postTrand.plugin.initlayui("page");
            },
            postrade:function(){
                if(!$(".text_check").hasClass("hide")){
                    window.location.href='../html/commodityDetails.html'
                }else{
                    alert("请选择一个商品")
                }
              // alert($(".text_check").hasClass("hide"))
            }

        },
        plugin: {
            initlayui: function (ele) {
                layui.use('laypage', function () {
                    var laypage = layui.laypage;

                    //执行一个laypage实例
                    laypage.render({
                        elem: 'page' //注意，这里的 test1 是 ID，不用加 # 号
                        , count: 500, //数据总数，从服务端得到
                        layout: ['prev', 'page', 'next', 'skip', 'limit'],
                        jump: function (obj, first) {
                            //obj包含了当前分页的所有参数，比如：
                            console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                            console.log(obj.limit); //得到每页显示的条数
                            //首次不执行
                            if (!first) {
                                layer.alert("当前页=" + obj.curr + ", 每页显示条数=" + obj.limit + "</br>" + " 我在returns.js第55行");
                            }
                        }
                    });
                });
            }
        }
    }
    _global = (function () {
        return this || (0, eval)("this")
    }());
    !("postTrand" in _global) && (_global.postTrand = postTrand);
})(window);
postTrand.init();