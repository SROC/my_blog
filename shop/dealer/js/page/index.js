/**
 *@desc
 *@author hdx
 *@date 2018/07/09 14:18:53
 *  供应商中心
 */

;(function(w,u){
    var _global = w;
    var index = {
        element:{
            radar:"score-chart",
            line:"sale-chart",
            layDate:"#datechoose"
        },
        init:function(){
            this.eventList.dataShow();
        },
        bind:function(){

        },
        eventList:{
            //数据展示
            dataShow:function(){
                //时间控件初始化
                index.plugin.layDateInit(index.element.layDate);
                var optionChart = index.eventList.getChartLineOption();
                var optionRadar = index.eventList.getChartRadarOption();
                index.plugin.chartInit(index.element.line,optionChart);
                index.plugin.chartInit(index.element.radar,optionRadar);
            },
            //获取chart图选项option
            getChartLineOption:function(){
                return option = {
                    title: {
                        text: "近一周统计",
                        textStyle: {
                            fontSize: "16"
                        }
                    },
                    xAxis: {
                        type: 'category',
                        data: ['周一', '周二', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                    },
                    yAxis: {
                        type: 'value',
                        name: "万元",
                        max: 60,
                        min: 0
                    },
                    series: [{
                        name: "test",
                        data: [20, 32, 10, 34, 29, 33, 32],
                        type: 'line',
                        symbolSize: 10,
                        symbolRotate: 10
                    }]
                };
            },
            getChartRadarOption:function(){
                return option = {
                    title : {
                    },
                    tooltip : {
                        enterable:true,
                        trigger: 'axis',
                        show:true
                    },
                    legend: {
                        orient : 'vertical',
                        x : 'right',
                        y : 'bottom',
                        data:['需求响应','描述相符']
                    },
                    polar : [
                        {
                            indicator : [
                                { text: '物流配送', max: 6000},
                                { text: '安装售后', max: 16000},
                                { text: '供货速度', max: 30000},
                                { text: '需求响应', max: 38000},
                                { text: '产品质量', max: 52000},
                            ],
                            radius:'50%'
                        }
                    ],
                    calculable : true,
                    series : [
                        {
                            type: 'radar',
                            symbolSize:"10",
                            data : [
                                {
                                    value : [4300, 10000, 28000, 35000, 50000, 19000],
                                    name : '需求响应'
                                },
                                {
                                    value : [5000, 14000, 28000, 31000, 42000, 21000],
                                    name : '描述相符'
                                },
                                {
                                    value : [4400, 11000, 25000, 30000, 50000, 18000],
                                    name : '描述相符'
                                }
                            ]
                        }
                    ]
                };
            }
        },
        plugin:{
          chartInit:function(elclass,option){
              var myChart = echarts.init(document.getElementById(elclass));
              myChart.setOption(option);
          },
            //layDate初始化
          layDateInit:function(el){
              laydate.render({
                  elem: el //指定元素
                  , type: 'datetime'
                  , range: true
              });
          }
        },
        interfaceList:{

        },
    }
    _global = (function(){
        return this||(0,eval)("this")
    }());
    !("index" in _global ) && (_global.index = index);
})(window);

index.init();