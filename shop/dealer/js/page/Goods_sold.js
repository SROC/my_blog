$('.allcheck').click(function(){
    if($(this).is(':checked')){
        $('.checkName').each(function(){
            $(this).find('input').attr("checked", true)
        })
    }else{
        $('.checkName').each(function(){
            $(this).find('input').attr("checked", false)
        })
    }
});

//分页
;(function (w, u) {
    var _global = w;
    var postTrand = {
        init: function () {
            this.bind()
            this.eventList.PluginInit()
        },
        bind: function () {
            $("._itemText").on("click", this.eventList.check)
        },
        eventList: {
            // 点击选中
            check: function () {
                if ($(".text_check").hasClass("hide")) {
                    $(".text_check").removeClass("hide");
                } else {
                    $(".text_check").addClass("hide");
                }
            },
            PluginInit:function(){
                postTrand.plugin.initlayui("page");
            }        },
        plugin: {
            initlayui: function (ele) {
                layui.use('laypage', function () {
                    var laypage = layui.laypage;

                    //执行一个laypage实例
                    laypage.render({
                        elem: 'page' //注意，这里的 test1 是 ID，不用加 # 号
                        , count: 500, //数据总数，从服务端得到
                        layout: ['prev', 'page', 'next', 'skip', 'limit'],
                        jump: function (obj, first) {
                            //obj包含了当前分页的所有参数，比如：
                            console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                            console.log(obj.limit); //得到每页显示的条数
                            //首次不执行
                            if (!first) {
                                layer.alert("当前页=" + obj.curr + ", 每页显示条数=" + obj.limit + "</br>" + " 我在returns.js第55行");
                            }
                        }
                    });
                });
            }
        }
    }
    _global = (function () {
        return this || (0, eval)("this")
    }());
    !("postTrand" in _global) && (_global.postTrand = postTrand);
})(window);
postTrand.init();