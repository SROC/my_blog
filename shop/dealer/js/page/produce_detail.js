getProvince();

/**
 * @author zsy
 * @date 2018/6/10
 * @Description:
 * shop_prodecedetail  产品详情js
 */
(function (w,u) {
   var $window = '';
   var parameter={};
   var data = {
        recommendList:[
            {
                "name" : '冰箱',
                "img" : 'http://b2b.haier.com/upload/f0a872bf-b883-4a0c-a1bd-86f3c3a71615/f729e1ea-96bf-4519-a92c-3588fd281b9c.png',
                "type" : '型号1'
            },
            {
                "name" : '彩电',
                "img" : 'http://b2b.haier.com/upload/f0a872bf-b883-4a0c-a1bd-86f3c3a71615/f729e1ea-96bf-4519-a92c-3588fd281b9c.png',
                "type" : '型号6'
            },
            {
                "name" : '洗衣机',
                "img" : 'http://b2b.haier.com/upload/f0a872bf-b883-4a0c-a1bd-86f3c3a71615/f729e1ea-96bf-4519-a92c-3588fd281b9c.png',
                "type" : '型号8'
            },
            {
                "name" : '辣鸡',
                "img" : 'http://b2b.haier.com/upload/f0a872bf-b883-4a0c-a1bd-86f3c3a71615/f729e1ea-96bf-4519-a92c-3588fd281b9c.png',
                "type" : '型号辣鸡'
            }
        ]
   };
   var shop_producedetail = {
       init:function () {
           this.bind();
           this.defaultEvent();
           // getProvinces();
       },
       defaultEvent:function () {
           _html.include.header();
           _html.include.footer();
           _html.include.otherRecommend({list:data.recommendList});
           _html.init();
       },
       bind:function () {
            $('.add').on('click',this.Events.add)
            $('.jian').on('click',this.Events.minus)
            $('.rig_deTabs ul li').on('click',this.Events.changeWhite);
            $('.proSamll ul li').on('click',this.Events.changeImg)
       },
       Events:{
            add:function () {
                var num =Number($('.number input').val()) ;
                $('.number input').val((num+1))
            },
            minus:function () {
                var num =Number($('.number input').val()) ;
                if(num<=1){
                    layer.alert('最少为一件商品',{icon:5})
                    return false
                }
                $('.number input').val((num-1))
            },
           changeWhite:function () {
               var index = $(this).index();
               $('.white').removeClass('white');
               $(this).addClass('white');
               $('.rig_deTabsImg li').addClass('hide');
               $('.rig_deTabsImg li').eq(index).removeClass('hide')
           },
           changeImg:function(){
                var src = $(this).children('img').attr('src');
                $('.proBig img').attr('src',src)
           }
       }
   };
    $window =  (function(){ return this||(0,eval)('this')}());
    !('shop_producedetail' in $window) &&  ($window.shop_producedetail = shop_producedetail);
})(window);
shop_producedetail.init();