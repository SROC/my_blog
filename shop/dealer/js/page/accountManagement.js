(function () {
    layui.use('table', function(){
        var table = layui.table;

        //展示已知数据
        table.render({
            elem: '#demo'
            ,cols: [[ //标题栏
                {type:'checkbox'},
                {field: 'username', title: '用户名', width: 120}
                ,{field: 'city', title: '部门', minWidth: 150}
                ,{field: 'sign', title: '角色', minWidth: 160}
            ]],
            data: [
                {
                    "username": "杜甫"
                    ,"city": "浙江杭州"
                    ,"sign": "人生恰似一场修行"
                },{
                    "username": "杜甫"
                    ,"city": "浙江杭州"
                    ,"sign": "人生恰似一场修行"
                },{
                    "username": "杜甫"
                    ,"city": "浙江杭州"
                    ,"sign": "人生恰似一场修行"
                },{
                    "username": "杜甫"
                    ,"city": "浙江杭州"
                    ,"sign": "人生恰似一场修行"
                },{
                    "username": "杜甫"
                    ,"city": "浙江杭州"
                    ,"sign": "人生恰似一场修行"
                },

            ]
            //,skin: 'line' //表格风格
            ,even: true
            //,page: true //是否显示分页
            //,limits: [5, 7, 10]
            //,limit: 5 //每页默认显示的数量
        });
    });
    //iframe窗
    $(".add-layui").on('click', function () {
        layer.open({
            type: 2,
            title: '账号管理',
            shadeClose: true,
            shade: false,
            maxmin: true, //开启最大化最小化按钮
            area: ['893px', '600px'],
            content: './account_add.html'
        });
    })


})()