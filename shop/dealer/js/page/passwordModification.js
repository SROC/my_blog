/**
 *@desc
 *@author sdp
 *@date 2018/07/22 14:18:53
 *  修改密码
 */

;(function(w,u){
    var _global = w;
    var reg=/[1-9][0-9]{4,}/;
    var re = new RegExp(reg);
    var flag1 = false
    var flag2 = false
    var flag3 = false
    var index = {
        init:function(){
            this.bind();
        },
        bind:function(){
            $("#password-old").on("blur", this.eventList.chengeRe)
            $("#password-new").on("blur", this.eventList.chengePass)
            $("#password-repeat").on("blur", this.eventList.chengeRepeat)
            $(".submtButton").on("click", this.eventList.submit)
        },
        eventList: {
            chengeRe:function(e){
                var content = $("#password-old").val();

                    if(re.test(content)){
                        $(".new-pass-tru").removeClass("layui-hide")
                        $(".new-pass-fals").addClass("layui-hide")
                        $(".flag1").text("")
                        flag1 = true
                    }else{
                        $(".new-pass-tru").addClass("layui-hide")
                        $(".new-pass-fals").removeClass("layui-hide")
                        flag1 = false
                    }


            },
            chengePass: function (e) {
                var content = $("#password-new").val();

                    if(re.test(content)){
                        $(".new-pass-true").removeClass("layui-hide")
                        $(".new-pass-false").addClass("layui-hide")
                        $(".flag2").text("")
                        flag2 = true
                    }else{
                        $(".new-pass-true").addClass("layui-hide")
                        $(".new-pass-false").removeClass("layui-hide")
                        flag2 = false
                    }

            },
            chengeRepeat: function (e) {
                var content = $("#password-repeat").val();
                var newpass = $("#password-new").val()

                    if(content == newpass){
                        $(".new-pass-truee").removeClass("layui-hide")
                        $(".new-pass-falsee").addClass("layui-hide")
                        $(".flag3").text("")
                        flag3 = true
                    }else{
                        $(".new-pass-truee").addClass("layui-hide")
                        $(".new-pass-falsee").removeClass("layui-hide")
                        flag3 = false
                    }

            },
            submit: function (e) {

                index.eventList.chengeRe()
                if(!flag1){
                    $(".flag1").text("原密码格式不正确")
                    return false
                }else{
                    $(".flag1").text("")
                }


                index.eventList.chengePass()
                if(!flag2){
                    $(".flag2").text("新密码格式不正确")
                    return false
                }else{
                    $(".flag2").text("")
                }


                index.eventList.chengeRepeat()
                if(!flag3){
                    $(".flag3").text("两次密码输入不一致")
                    return false
                }else{
                    $(".flag3").text("")
                }


                layer.open({
                    title: '提示',
                    content: '保存成功'
                });
            }
        }

    }
    _global = (function(){
        return this||(0,eval)("this")
    }());
    !("index" in _global ) && (_global.index = index);
})(window);

index.init();
