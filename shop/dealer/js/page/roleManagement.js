(function () {
    layui.use('table', function(){
        var table = layui.table;

        //展示已知数据
        table.render({
            elem: '#demo'
            ,cols: [[ //标题栏
                {type:'checkbox'},
                {field: 'username', title: '角色名称', width: 120},
                {field: 'available', title: '是否可用', minWidth: 150},
                {field: 'menuPermissions', title: '菜单权限', minWidth: 160}
            ]],
            data: [
                {
                    "username": "平台运营",
                    "available": "可用",
                    "menuPermissions": "无"
                },
                {
                    "username": "平台运营",
                    "available": "可用",
                    "menuPermissions": "无"
                },
                {
                    "username": "平台运营",
                    "available": "可用",
                    "menuPermissions": "无"
                }],
            even: true
        });
    });
    //iframe窗
$(".add-layui").on('click', function () {
    layer.open({
        type: 2,
        title: '角色管理',
        shadeClose: true,
        shade: false,
        maxmin: true, //开启最大化最小化按钮
        area: ['893px', '600px'],
        content: './role_add.html'
    });
})


})()