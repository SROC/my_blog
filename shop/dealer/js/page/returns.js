/**
 *@desc
 *@author hdx
 *@date 2018/07/10 15:23:05
 * 订单列表
 */

;(function(w,u){
    var _global = w;
    var returns = {
        element:{
            orderstate:"0",
        },
        //初始化
        init:function(){
            this.bind();
            this.evnetList.pluginInit();
        },
        //绑定
        bind:function(){
            $(".u-order li").on("click",this.evnetList.changeTab)
        },
        //事件列表
        evnetList:{
            //切换tab
            changeTab:function(){
                var index = $(this).index();
                $(this).addClass("active").siblings().removeClass("active");
                switch (index){
                    case 0:
                }
            },
            pluginInit:function(){
                returns.plugin.initlayui("page");
                returns.plugin.initlaydate(".starttime");
                returns.plugin.initlaydate(".endtime");
            }
        },
        interfaceList:{

        },
        //插件管理
        plugin:{
            initlaydate:function(ele){
                laydate.render({
                    elem: ele
                    ,type: 'datetime'
                });
            },
            initlayui:function(ele){
                layui.use('laypage', function(){
                    var laypage = layui.laypage;

                    //执行一个laypage实例
                    laypage.render({
                        elem: 'page' //注意，这里的 test1 是 ID，不用加 # 号
                        ,count: 500, //数据总数，从服务端得到
                        layout:['prev', 'page', 'next','skip','limit'],
                        jump: function(obj, first){
                            //obj包含了当前分页的所有参数，比如：
                            console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                            console.log(obj.limit); //得到每页显示的条数
                            //首次不执行
                            if(!first){
                               layer.alert("当前页="+ obj.curr +", 每页显示条数=" + obj.limit + "</br>"+ " 我在returns.js第55行");
                             }
                        }
                    });
                });
            }
        }
    }
    _global = (function(){
        return this||(0,eval)("this");
    }());
    !("returns" in _global)&& (_global.returns = returns);
})(window);

returns.init();
