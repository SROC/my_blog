/*TMODJS:{"version":8,"md5":"f109a5909d2e8843d9b89726c4ae97fc"}*/
template('common/page/shop_productList/sidebarItem',function($data,$filename
/*``*/) {
'use strict';var $utils=this,$helpers=$utils.$helpers,$each=$utils.$each,list=$data.list,value=$data.value,index=$data.index,$escape=$utils.$escape,$out='';$each(list,function(value,index){
$out+=' <div class="_sidebar_item"> <div class="productImg"> <img src="';
$out+=$escape(value.imgUrl);
$out+='" alt=""> </div> <p class="productName">';
$out+=$escape(value.productTitle);
$out+='</p> <p class="productPrice">￥';
$out+=$escape(value.price);
$out+='</p> <p class="haveEvaluation">已有99999人评价</p> </div> ';
});
return new String($out);
});