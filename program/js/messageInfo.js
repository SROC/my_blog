(function () {
    // 点击回复
    $(".reply-top-option-reply").on("click", function () {
        $(".reply-top-option-reply-block").show();
        $(".header-submit").show()
    })
    // 点击背景隐藏
    $(".reply-block-bg").on('click', function (event) {
        // 禁用子级点击事件冒泡
        if(event.target==this){
            $(".reply-top-option-reply-block").hide()
            $(".header-submit").hide()
        }

    })
})()