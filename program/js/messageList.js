(function () {
    $(".message").pullToRefresh(function () {
// 下拉刷新触发时执行的操作放这里。
// 从 v1.1.2 版本才支持回调函数，之前的版本只能通过事件监听
    });
    $(".message").on("pull-to-refresh", function() {
        getMessageList()
        $(".message").pullToRefreshDone();
    });


    getMessageList()
    // 获取消息列表
    function getMessageList() {
        $.showLoading();
        $.ajax({
            //请求方式
            type: "GET",
            //文件位置
            url: "./public/json/messageList.json",
            //返回数据格式为json,也可以是其他格式如
            dataType: "json",
            //请求成功后要执行的函数，拼接html
            success: function (res) {
                console.log(res)
                var list = res.messageList

                list.forEach(function (obj) {
                    obj.is_read = obj.is_read == 1 ? true : false
                })
                console.log(list)

                if (list && list.length > 0) {
                    $$.render('.message', list, 'tpl/messageList.html').then(function () {
                        // 点击编辑按钮时
                        $(".weui-emit").on('click', function () {
                            $(".weui-emit").hide()
                            $(".weui-emit-submit").show()
                            $(".msgLister-check").removeClass("input-hide")
                            $(".message-delect").show()
                            $(".title-click").unbind('click');
                            $(".message").css('padding-bottom','50px');
                        })
                        // 点击完成按钮时
                        $(".weui-emit-submit").on('click', function () {
                            $(".weui-emit-submit").hide()
                            $(".weui-emit").show()
                            $(".msgLister-check").addClass("input-hide")
                            $(".message-delect").hide()
                            $(".title").addClass("title-click")
                            $(".title-click").on('click', function () {
                                var msgId = $(this).data("msgid");
                                location.href = 'messageInfo.html?msgId='+ msgId
                            })
                            $("[name='msgId']").removeAttr("checked");
                            $(".message").css('padding-bottom','0');
                        })
                        //点击标题跳转
                        $(".title-click").on('click', function () {
                            var msgId = $(this).data("msgid");
                            location.href = 'messageInfo.html?msgId='+ msgId
                        })
                        //点击删除按钮
                        $(".message-delect").on('click', function () {
                            if ($('input:checkbox[name=msgId]:checked').size() != 0) {
                                $.confirm('你要删除' + $('input:checkbox[name=msgId]:checked').size() + '条数据吗', function() {
                                    //点击确认后的回调函数
                                    $.toast("操作成功", function () {
                                        getMessageList()
                                    });
                                }, function() {
                                    //点击取消后的回调函数
                                });
                            }
                        })
                    })
                } else {
                    $('.message').html(`<div class="weui-loadmore weui-loadmore_line">
                <span class="weui-loadmore__tips">暂无数据</span>
            </div>`)
                }
                $.hideLoading();
            }
        })
    }
})()