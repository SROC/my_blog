
function getHeader(url) {
    let header = {}
    if ($$.getParams().accessToken || sessionStorage.accessToken) {
        header.accessToken = $$.getParams().accessToken || sessionStorage.accessToken
        return header
    } else {
        if (url.includes('/api/member')) {
//             header.accessToken = '1'
            getAccessToken()
        }
        return false
    }

}

const $$ = {
    /**
     * 模板渲染
     *
     * @param {String} selecter 标准选择器
     * @param {Object} data 数据
     * @param {String} url 模板路径或模板字符串
     *
     * @returns {Promise} 渲染信息或错误信息
     */
    render(selecter, data, url) {
        let index = 0
        data.forEach(function (obj) {
            obj.index = index++
        })
        return new Promise(function (resolve, reject) {

            if (url.endsWith('.html')) {
                $.showLoading('请稍后')

                $.ajax(url + '?v=' + new Date().getTime()).then(function (html) {

                    $(selecter).html(Mustache.render(html, {
                        data: data
                    }))
                    $.hideLoading();
                    resolve({
                        html: html,
                        data: data,
                        tpl: url,
                        selecter: selecter
                    })
                })
            } else {

                let html = url
                $(selecter).html(Mustache.render(html, {
                    data: data
                }))
                $.hideLoading();
                resolve({
                    html: html,
                    data: data,
                    selecter: selecter
                })
            }
        });
    },
    /**
     * 获得url参数
     *
     * @param {String} url 如果没有则为当前url
     *
     * @returns {Object} json的形式
     */
    getParams(url = window.location.href) {
        let params = {}
        url = decodeURIComponent(url)
        let sp = url.includes('&amp;') ? '&amp;' : '&'

        if (url.split('?').length < 2) {
            return params
        }

        let paramArray = url.split('?')[1].split(sp)

        if (paramArray && paramArray.length > 0) {
            for (let i = 0; i < paramArray.length; i++) {
                paramArray[i].split('=')
                params[paramArray[i].split('=')[0]] = decodeURI(paramArray[i].split('=')[1])
            }
        }

        params.accessToken ? sessionStorage.accessToken = params.accessToken : '';

        return params
    },
    get(url) {
        if ($('.weui-toast')) {
            $.showLoading('请稍后')
        }

        let p = $.ajax({
            url: url,
            type: 'GET',
            headers: getHeader(url)
        })
        let q = p
        q.then(function (res) {
            if (res.errorCode == 9999) {
                getAccessToken()
            }
            $.hideLoading();
        }).catch(function () {
            $.hideLoading();
        })
        return p
    },
    post(url, data = {}, contentType = 'application/x-www-form-urlencoded') {
        if ($('.weui-toast')) {
            $.showLoading('请稍后')
        }
        console.log(data)
        let p = $.ajax({
            url: url,
            type: 'POST',
            data: data,
            contentType: contentType,
            headers: getHeader(url)
        })
        let q = p
        q.then(function (res) {
            if (res.errorCode == 9999) {
                getAccessToken()
            }
            $.hideLoading();
        }).catch(function () {
            $.hideLoading();
        })
        return p
    },
    put(url, data) {
        if ($('.weui-toast')) {
            $.showLoading('请稍后')
        }
        let p = $.ajax({
            url: url,
            type: 'PUT',
            data: data,
            headers: getHeader(url)
        })
        let q = p
        q.then(function (res) {
            if (res.errorCode == 9999) {
                getAccessToken()
            }
            $.hideLoading();
        }).catch(function () {
            $.hideLoading();
        })
        return p
    }
}